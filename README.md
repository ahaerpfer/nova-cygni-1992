# Nova Cygni 1992 (V1974 Cyg)

This is an old dataset with [Nova Cygni 1992 magnitude
estimates](ahaerpfer-usenet.txt) starting right with the original
discovery observation.  My original usenet post is still archived at
Google groups:

* [Nova Cygni 1992 - SUMMARY of Magnitude Estimates
  II][ahaerpfer-usenet]

Annuk et al. made use of this lightcurve data in their A&A publication:

* [Annuk, K., Kolka, I., & Leedjarv, L.: Nova Cygni 1992 in the
  post-maximum period][annuk-aa], Astronomy and Astrophysics (ISSN
  0004-6361), vol. 269, no. 1-2, p. L5-L8.

The provided [Jupyter notebook](nova-cygni-1992-lightcurve.ipynb)
creates the following lightcurve plot directly from the data in the
usenet posting:

![Nova Cygni 1992 lightcurve](lightcurve.png)


[ahaerpfer-usenet]: http://groups.google.com/groups?as_umsgid=ug141bb.701543474%40sun6
[annuk-aa]: http://adsabs.harvard.edu/full/1993A%26A...269L...5A
